package com.gildedrose;

class GildedRose {
    Item[] items;

    public GildedRose(Item[] items) {
        this.items = items;
    }

    public void updateQuality() {
        for (int i = 0; i < items.length; i++) {
            Item item = items[i];

            // Could be added to a list with things that should be ignored
            if(item.name.equals("Sulfuras, Hand of Ragnaros"))
               continue;

            item.sellIn--;

            switch (item.name){
                case "Aged Brie": handleBrie(item); break;
                case "Backstage passes to a TAFKAL80ETC concert": handleBackStage(item); break;
                default: handleDefaultItems(item); break;
            }

            if(item.quality>50)
                item.quality = 50;
        }
    }

    private void handleDefaultItems(Item item) {
        item.quality -= item.sellIn >= 0 ? 1 : 2;
    }

    private void handleBackStage(Item item) {
        if(item.sellIn < 0){
            item.quality = 0;
            return;
        }
        if(item.sellIn <= 5){
            item.quality += 3;
            return;
        }
        if(item.sellIn <= 10){
            item.quality += 2;
            return;
        }
        item.quality++;
    }

    private void handleBrie(Item item) {
        item.quality += item.sellIn >= 0 ? 1 : 2;
    }



}