Feature: Mess
  I don't know

  Scenario Outline: Aged Brie
    Given an item named <itemName> with quality <quality> and which has to be sold in <sellIn> days
    When a day has passed
    Then it has to be sold in <newSellIn> days
    And its quality is <newQuality>

    Examples:
    | itemName | quality | sellIn | newSellIn | newQuality |
    | "Aged Brie" | 9 | 11 | 10 | 10 |
    | "Aged Brie" | 50 | 5 | 4 | 50 |
    | "Aged Brie" | 9 | -1 | -2 | 11 |
    | "Cheddar" | 9 | 11 | 10 | 8 |
    | "Cheddar" | 9 | -1 | -2 | 7 |
    | "Sulfuras, Hand of Ragnaros" | 50 | 10 | 10 | 50 |
#    | "Sulfuras, Hand of Ragnaros" | 49 | 10 | 10 | 49 |
    | "Backstage passes to a TAFKAL80ETC concert" | 10 | 12 | 11 | 11 |
    | "Backstage passes to a TAFKAL80ETC concert" | 10 | 10 | 9 | 12 |
    | "Backstage passes to a TAFKAL80ETC concert" | 10 | 5 | 4 | 13 |
    | "Backstage passes to a TAFKAL80ETC concert" | 10 | 0 | -1 | 0 |
    | "Backstage passes to a TAFKAL80ETC concert" | 50 | 5 | 4 | 50 |

